/*
 * generated by Xtext 2.17.1
 */
package fr.n7.simplepdl.txt.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Alternatives;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.CrossReference;
import org.eclipse.xtext.EnumLiteralDeclaration;
import org.eclipse.xtext.EnumRule;
import org.eclipse.xtext.Grammar;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.Group;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.TerminalRule;
import org.eclipse.xtext.common.services.TerminalsGrammarAccess;
import org.eclipse.xtext.service.AbstractElementFinder.AbstractEnumRuleElementFinder;
import org.eclipse.xtext.service.AbstractElementFinder.AbstractGrammarElementFinder;
import org.eclipse.xtext.service.GrammarProvider;

@Singleton
public class PDLGrammarAccess extends AbstractGrammarElementFinder {
	
	public class ProcessElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "fr.n7.simplepdl.txt.PDL.Process");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cProcessAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cProcessKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cNameIDTerminalRuleCall_2_0 = (RuleCall)cNameAssignment_2.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Assignment cProcessElementsAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final RuleCall cProcessElementsProcessElementParserRuleCall_4_0 = (RuleCall)cProcessElementsAssignment_4.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_5 = (Keyword)cGroup.eContents().get(5);
		
		//Process:
		//	{Process}
		//	'process' name=ID '{'
		//	processElements+=ProcessElement*
		//	'}';
		@Override public ParserRule getRule() { return rule; }
		
		//{Process} 'process' name=ID '{' processElements+=ProcessElement* '}'
		public Group getGroup() { return cGroup; }
		
		//{Process}
		public Action getProcessAction_0() { return cProcessAction_0; }
		
		//'process'
		public Keyword getProcessKeyword_1() { return cProcessKeyword_1; }
		
		//name=ID
		public Assignment getNameAssignment_2() { return cNameAssignment_2; }
		
		//ID
		public RuleCall getNameIDTerminalRuleCall_2_0() { return cNameIDTerminalRuleCall_2_0; }
		
		//'{'
		public Keyword getLeftCurlyBracketKeyword_3() { return cLeftCurlyBracketKeyword_3; }
		
		//processElements+=ProcessElement*
		public Assignment getProcessElementsAssignment_4() { return cProcessElementsAssignment_4; }
		
		//ProcessElement
		public RuleCall getProcessElementsProcessElementParserRuleCall_4_0() { return cProcessElementsProcessElementParserRuleCall_4_0; }
		
		//'}'
		public Keyword getRightCurlyBracketKeyword_5() { return cRightCurlyBracketKeyword_5; }
	}
	public class ProcessElementElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "fr.n7.simplepdl.txt.PDL.ProcessElement");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cWorkDefinitionParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cWorkSequenceParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final RuleCall cGuidanceParserRuleCall_2 = (RuleCall)cAlternatives.eContents().get(2);
		private final RuleCall cRessourceParserRuleCall_3 = (RuleCall)cAlternatives.eContents().get(3);
		private final RuleCall cRequiredQuantityParserRuleCall_4 = (RuleCall)cAlternatives.eContents().get(4);
		
		//ProcessElement:
		//	WorkDefinition | WorkSequence | Guidance | Ressource | RequiredQuantity;
		@Override public ParserRule getRule() { return rule; }
		
		//WorkDefinition | WorkSequence | Guidance | Ressource | RequiredQuantity
		public Alternatives getAlternatives() { return cAlternatives; }
		
		//WorkDefinition
		public RuleCall getWorkDefinitionParserRuleCall_0() { return cWorkDefinitionParserRuleCall_0; }
		
		//WorkSequence
		public RuleCall getWorkSequenceParserRuleCall_1() { return cWorkSequenceParserRuleCall_1; }
		
		//Guidance
		public RuleCall getGuidanceParserRuleCall_2() { return cGuidanceParserRuleCall_2; }
		
		//Ressource
		public RuleCall getRessourceParserRuleCall_3() { return cRessourceParserRuleCall_3; }
		
		//RequiredQuantity
		public RuleCall getRequiredQuantityParserRuleCall_4() { return cRequiredQuantityParserRuleCall_4; }
	}
	public class EStringElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "fr.n7.simplepdl.txt.PDL.EString");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cSTRINGTerminalRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cIDTerminalRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		
		//EString:
		//	STRING | ID;
		@Override public ParserRule getRule() { return rule; }
		
		//STRING | ID
		public Alternatives getAlternatives() { return cAlternatives; }
		
		//STRING
		public RuleCall getSTRINGTerminalRuleCall_0() { return cSTRINGTerminalRuleCall_0; }
		
		//ID
		public RuleCall getIDTerminalRuleCall_1() { return cIDTerminalRuleCall_1; }
	}
	public class WorkDefinitionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "fr.n7.simplepdl.txt.PDL.WorkDefinition");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cWorkDefinitionAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cWdKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cNameIDTerminalRuleCall_2_0 = (RuleCall)cNameAssignment_2.eContents().get(0);
		
		//WorkDefinition:
		//	{WorkDefinition}
		//	'wd' name=ID;
		@Override public ParserRule getRule() { return rule; }
		
		//{WorkDefinition} 'wd' name=ID
		public Group getGroup() { return cGroup; }
		
		//{WorkDefinition}
		public Action getWorkDefinitionAction_0() { return cWorkDefinitionAction_0; }
		
		//'wd'
		public Keyword getWdKeyword_1() { return cWdKeyword_1; }
		
		//name=ID
		public Assignment getNameAssignment_2() { return cNameAssignment_2; }
		
		//ID
		public RuleCall getNameIDTerminalRuleCall_2_0() { return cNameIDTerminalRuleCall_2_0; }
	}
	public class WorkSequenceElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "fr.n7.simplepdl.txt.PDL.WorkSequence");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cWsKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cLinkTypeAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cLinkTypeWorkSequenceTypeEnumRuleCall_1_0 = (RuleCall)cLinkTypeAssignment_1.eContents().get(0);
		private final Keyword cFromKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Assignment cPredecessorAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final CrossReference cPredecessorWorkDefinitionCrossReference_3_0 = (CrossReference)cPredecessorAssignment_3.eContents().get(0);
		private final RuleCall cPredecessorWorkDefinitionIDTerminalRuleCall_3_0_1 = (RuleCall)cPredecessorWorkDefinitionCrossReference_3_0.eContents().get(1);
		private final Keyword cToKeyword_4 = (Keyword)cGroup.eContents().get(4);
		private final Assignment cSuccessorAssignment_5 = (Assignment)cGroup.eContents().get(5);
		private final CrossReference cSuccessorWorkDefinitionCrossReference_5_0 = (CrossReference)cSuccessorAssignment_5.eContents().get(0);
		private final RuleCall cSuccessorWorkDefinitionIDTerminalRuleCall_5_0_1 = (RuleCall)cSuccessorWorkDefinitionCrossReference_5_0.eContents().get(1);
		
		//WorkSequence:
		//	'ws' linkType=WorkSequenceType
		//	'from' predecessor=[WorkDefinition]
		//	'to' successor=[WorkDefinition];
		@Override public ParserRule getRule() { return rule; }
		
		//'ws' linkType=WorkSequenceType 'from' predecessor=[WorkDefinition] 'to' successor=[WorkDefinition]
		public Group getGroup() { return cGroup; }
		
		//'ws'
		public Keyword getWsKeyword_0() { return cWsKeyword_0; }
		
		//linkType=WorkSequenceType
		public Assignment getLinkTypeAssignment_1() { return cLinkTypeAssignment_1; }
		
		//WorkSequenceType
		public RuleCall getLinkTypeWorkSequenceTypeEnumRuleCall_1_0() { return cLinkTypeWorkSequenceTypeEnumRuleCall_1_0; }
		
		//'from'
		public Keyword getFromKeyword_2() { return cFromKeyword_2; }
		
		//predecessor=[WorkDefinition]
		public Assignment getPredecessorAssignment_3() { return cPredecessorAssignment_3; }
		
		//[WorkDefinition]
		public CrossReference getPredecessorWorkDefinitionCrossReference_3_0() { return cPredecessorWorkDefinitionCrossReference_3_0; }
		
		//ID
		public RuleCall getPredecessorWorkDefinitionIDTerminalRuleCall_3_0_1() { return cPredecessorWorkDefinitionIDTerminalRuleCall_3_0_1; }
		
		//'to'
		public Keyword getToKeyword_4() { return cToKeyword_4; }
		
		//successor=[WorkDefinition]
		public Assignment getSuccessorAssignment_5() { return cSuccessorAssignment_5; }
		
		//[WorkDefinition]
		public CrossReference getSuccessorWorkDefinitionCrossReference_5_0() { return cSuccessorWorkDefinitionCrossReference_5_0; }
		
		//ID
		public RuleCall getSuccessorWorkDefinitionIDTerminalRuleCall_5_0_1() { return cSuccessorWorkDefinitionIDTerminalRuleCall_5_0_1; }
	}
	public class GuidanceElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "fr.n7.simplepdl.txt.PDL.Guidance");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cNoteKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cTextAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cTextSTRINGTerminalRuleCall_1_0 = (RuleCall)cTextAssignment_1.eContents().get(0);
		
		//Guidance:
		//	'note' text=STRING;
		@Override public ParserRule getRule() { return rule; }
		
		//'note' text=STRING
		public Group getGroup() { return cGroup; }
		
		//'note'
		public Keyword getNoteKeyword_0() { return cNoteKeyword_0; }
		
		//text=STRING
		public Assignment getTextAssignment_1() { return cTextAssignment_1; }
		
		//STRING
		public RuleCall getTextSTRINGTerminalRuleCall_1_0() { return cTextSTRINGTerminalRuleCall_1_0; }
	}
	public class RessourceElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "fr.n7.simplepdl.txt.PDL.Ressource");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cRessourceAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cRessourceKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cNameEStringParserRuleCall_2_0 = (RuleCall)cNameAssignment_2.eContents().get(0);
		private final Keyword cQuantityKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Assignment cQuantityAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final RuleCall cQuantityEIntParserRuleCall_4_0 = (RuleCall)cQuantityAssignment_4.eContents().get(0);
		
		//Ressource:
		//	{Ressource}
		//	'ressource' name=EString 'quantity' quantity=EInt;
		@Override public ParserRule getRule() { return rule; }
		
		//{Ressource} 'ressource' name=EString 'quantity' quantity=EInt
		public Group getGroup() { return cGroup; }
		
		//{Ressource}
		public Action getRessourceAction_0() { return cRessourceAction_0; }
		
		//'ressource'
		public Keyword getRessourceKeyword_1() { return cRessourceKeyword_1; }
		
		//name=EString
		public Assignment getNameAssignment_2() { return cNameAssignment_2; }
		
		//EString
		public RuleCall getNameEStringParserRuleCall_2_0() { return cNameEStringParserRuleCall_2_0; }
		
		//'quantity'
		public Keyword getQuantityKeyword_3() { return cQuantityKeyword_3; }
		
		//quantity=EInt
		public Assignment getQuantityAssignment_4() { return cQuantityAssignment_4; }
		
		//EInt
		public RuleCall getQuantityEIntParserRuleCall_4_0() { return cQuantityEIntParserRuleCall_4_0; }
	}
	public class RequiredQuantityElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "fr.n7.simplepdl.txt.PDL.RequiredQuantity");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cRequesterKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cRequesterAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final CrossReference cRequesterWorkDefinitionCrossReference_1_0 = (CrossReference)cRequesterAssignment_1.eContents().get(0);
		private final RuleCall cRequesterWorkDefinitionEStringParserRuleCall_1_0_1 = (RuleCall)cRequesterWorkDefinitionCrossReference_1_0.eContents().get(1);
		private final Keyword cAsksKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Assignment cRequestAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cRequestEIntParserRuleCall_3_0 = (RuleCall)cRequestAssignment_3.eContents().get(0);
		private final Keyword cOfKeyword_4 = (Keyword)cGroup.eContents().get(4);
		private final Assignment cRessourceAssignment_5 = (Assignment)cGroup.eContents().get(5);
		private final CrossReference cRessourceRessourceCrossReference_5_0 = (CrossReference)cRessourceAssignment_5.eContents().get(0);
		private final RuleCall cRessourceRessourceEStringParserRuleCall_5_0_1 = (RuleCall)cRessourceRessourceCrossReference_5_0.eContents().get(1);
		
		//RequiredQuantity:
		//	'requester' requester=[WorkDefinition|EString]
		//	'asks' request=EInt
		//	'of' ressource=[Ressource|EString];
		@Override public ParserRule getRule() { return rule; }
		
		//'requester' requester=[WorkDefinition|EString] 'asks' request=EInt 'of' ressource=[Ressource|EString]
		public Group getGroup() { return cGroup; }
		
		//'requester'
		public Keyword getRequesterKeyword_0() { return cRequesterKeyword_0; }
		
		//requester=[WorkDefinition|EString]
		public Assignment getRequesterAssignment_1() { return cRequesterAssignment_1; }
		
		//[WorkDefinition|EString]
		public CrossReference getRequesterWorkDefinitionCrossReference_1_0() { return cRequesterWorkDefinitionCrossReference_1_0; }
		
		//EString
		public RuleCall getRequesterWorkDefinitionEStringParserRuleCall_1_0_1() { return cRequesterWorkDefinitionEStringParserRuleCall_1_0_1; }
		
		//'asks'
		public Keyword getAsksKeyword_2() { return cAsksKeyword_2; }
		
		//request=EInt
		public Assignment getRequestAssignment_3() { return cRequestAssignment_3; }
		
		//EInt
		public RuleCall getRequestEIntParserRuleCall_3_0() { return cRequestEIntParserRuleCall_3_0; }
		
		//'of'
		public Keyword getOfKeyword_4() { return cOfKeyword_4; }
		
		//ressource=[Ressource|EString]
		public Assignment getRessourceAssignment_5() { return cRessourceAssignment_5; }
		
		//[Ressource|EString]
		public CrossReference getRessourceRessourceCrossReference_5_0() { return cRessourceRessourceCrossReference_5_0; }
		
		//EString
		public RuleCall getRessourceRessourceEStringParserRuleCall_5_0_1() { return cRessourceRessourceEStringParserRuleCall_5_0_1; }
	}
	public class EIntElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "fr.n7.simplepdl.txt.PDL.EInt");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cHyphenMinusKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final RuleCall cINTTerminalRuleCall_1 = (RuleCall)cGroup.eContents().get(1);
		
		//EInt ecore::EInt:
		//	'-'? INT;
		@Override public ParserRule getRule() { return rule; }
		
		//'-'? INT
		public Group getGroup() { return cGroup; }
		
		//'-'?
		public Keyword getHyphenMinusKeyword_0() { return cHyphenMinusKeyword_0; }
		
		//INT
		public RuleCall getINTTerminalRuleCall_1() { return cINTTerminalRuleCall_1; }
	}
	
	public class WorkSequenceTypeElements extends AbstractEnumRuleElementFinder {
		private final EnumRule rule = (EnumRule) GrammarUtil.findRuleForName(getGrammar(), "fr.n7.simplepdl.txt.PDL.WorkSequenceType");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final EnumLiteralDeclaration cStartToStartEnumLiteralDeclaration_0 = (EnumLiteralDeclaration)cAlternatives.eContents().get(0);
		private final Keyword cStartToStartStartToStartKeyword_0_0 = (Keyword)cStartToStartEnumLiteralDeclaration_0.eContents().get(0);
		private final EnumLiteralDeclaration cFinishToStartEnumLiteralDeclaration_1 = (EnumLiteralDeclaration)cAlternatives.eContents().get(1);
		private final Keyword cFinishToStartFinishToStartKeyword_1_0 = (Keyword)cFinishToStartEnumLiteralDeclaration_1.eContents().get(0);
		private final EnumLiteralDeclaration cStartToFinishEnumLiteralDeclaration_2 = (EnumLiteralDeclaration)cAlternatives.eContents().get(2);
		private final Keyword cStartToFinishStartToFinishKeyword_2_0 = (Keyword)cStartToFinishEnumLiteralDeclaration_2.eContents().get(0);
		private final EnumLiteralDeclaration cFinishToFinishEnumLiteralDeclaration_3 = (EnumLiteralDeclaration)cAlternatives.eContents().get(3);
		private final Keyword cFinishToFinishFinishToFinishKeyword_3_0 = (Keyword)cFinishToFinishEnumLiteralDeclaration_3.eContents().get(0);
		
		//enum WorkSequenceType:
		//	startToStart | finishToStart | startToFinish | finishToFinish;
		public EnumRule getRule() { return rule; }
		
		//startToStart | finishToStart | startToFinish | finishToFinish
		public Alternatives getAlternatives() { return cAlternatives; }
		
		//startToStart
		public EnumLiteralDeclaration getStartToStartEnumLiteralDeclaration_0() { return cStartToStartEnumLiteralDeclaration_0; }
		
		//'startToStart'
		public Keyword getStartToStartStartToStartKeyword_0_0() { return cStartToStartStartToStartKeyword_0_0; }
		
		//finishToStart
		public EnumLiteralDeclaration getFinishToStartEnumLiteralDeclaration_1() { return cFinishToStartEnumLiteralDeclaration_1; }
		
		//'finishToStart'
		public Keyword getFinishToStartFinishToStartKeyword_1_0() { return cFinishToStartFinishToStartKeyword_1_0; }
		
		//startToFinish
		public EnumLiteralDeclaration getStartToFinishEnumLiteralDeclaration_2() { return cStartToFinishEnumLiteralDeclaration_2; }
		
		//'startToFinish'
		public Keyword getStartToFinishStartToFinishKeyword_2_0() { return cStartToFinishStartToFinishKeyword_2_0; }
		
		//finishToFinish
		public EnumLiteralDeclaration getFinishToFinishEnumLiteralDeclaration_3() { return cFinishToFinishEnumLiteralDeclaration_3; }
		
		//'finishToFinish'
		public Keyword getFinishToFinishFinishToFinishKeyword_3_0() { return cFinishToFinishFinishToFinishKeyword_3_0; }
	}
	
	private final ProcessElements pProcess;
	private final ProcessElementElements pProcessElement;
	private final EStringElements pEString;
	private final WorkDefinitionElements pWorkDefinition;
	private final WorkSequenceElements pWorkSequence;
	private final GuidanceElements pGuidance;
	private final RessourceElements pRessource;
	private final RequiredQuantityElements pRequiredQuantity;
	private final WorkSequenceTypeElements eWorkSequenceType;
	private final EIntElements pEInt;
	
	private final Grammar grammar;
	
	private final TerminalsGrammarAccess gaTerminals;

	@Inject
	public PDLGrammarAccess(GrammarProvider grammarProvider,
			TerminalsGrammarAccess gaTerminals) {
		this.grammar = internalFindGrammar(grammarProvider);
		this.gaTerminals = gaTerminals;
		this.pProcess = new ProcessElements();
		this.pProcessElement = new ProcessElementElements();
		this.pEString = new EStringElements();
		this.pWorkDefinition = new WorkDefinitionElements();
		this.pWorkSequence = new WorkSequenceElements();
		this.pGuidance = new GuidanceElements();
		this.pRessource = new RessourceElements();
		this.pRequiredQuantity = new RequiredQuantityElements();
		this.eWorkSequenceType = new WorkSequenceTypeElements();
		this.pEInt = new EIntElements();
	}
	
	protected Grammar internalFindGrammar(GrammarProvider grammarProvider) {
		Grammar grammar = grammarProvider.getGrammar(this);
		while (grammar != null) {
			if ("fr.n7.simplepdl.txt.PDL".equals(grammar.getName())) {
				return grammar;
			}
			List<Grammar> grammars = grammar.getUsedGrammars();
			if (!grammars.isEmpty()) {
				grammar = grammars.iterator().next();
			} else {
				return null;
			}
		}
		return grammar;
	}
	
	@Override
	public Grammar getGrammar() {
		return grammar;
	}
	
	
	public TerminalsGrammarAccess getTerminalsGrammarAccess() {
		return gaTerminals;
	}

	
	//Process:
	//	{Process}
	//	'process' name=ID '{'
	//	processElements+=ProcessElement*
	//	'}';
	public ProcessElements getProcessAccess() {
		return pProcess;
	}
	
	public ParserRule getProcessRule() {
		return getProcessAccess().getRule();
	}
	
	//ProcessElement:
	//	WorkDefinition | WorkSequence | Guidance | Ressource | RequiredQuantity;
	public ProcessElementElements getProcessElementAccess() {
		return pProcessElement;
	}
	
	public ParserRule getProcessElementRule() {
		return getProcessElementAccess().getRule();
	}
	
	//EString:
	//	STRING | ID;
	public EStringElements getEStringAccess() {
		return pEString;
	}
	
	public ParserRule getEStringRule() {
		return getEStringAccess().getRule();
	}
	
	//WorkDefinition:
	//	{WorkDefinition}
	//	'wd' name=ID;
	public WorkDefinitionElements getWorkDefinitionAccess() {
		return pWorkDefinition;
	}
	
	public ParserRule getWorkDefinitionRule() {
		return getWorkDefinitionAccess().getRule();
	}
	
	//WorkSequence:
	//	'ws' linkType=WorkSequenceType
	//	'from' predecessor=[WorkDefinition]
	//	'to' successor=[WorkDefinition];
	public WorkSequenceElements getWorkSequenceAccess() {
		return pWorkSequence;
	}
	
	public ParserRule getWorkSequenceRule() {
		return getWorkSequenceAccess().getRule();
	}
	
	//Guidance:
	//	'note' text=STRING;
	public GuidanceElements getGuidanceAccess() {
		return pGuidance;
	}
	
	public ParserRule getGuidanceRule() {
		return getGuidanceAccess().getRule();
	}
	
	//Ressource:
	//	{Ressource}
	//	'ressource' name=EString 'quantity' quantity=EInt;
	public RessourceElements getRessourceAccess() {
		return pRessource;
	}
	
	public ParserRule getRessourceRule() {
		return getRessourceAccess().getRule();
	}
	
	//RequiredQuantity:
	//	'requester' requester=[WorkDefinition|EString]
	//	'asks' request=EInt
	//	'of' ressource=[Ressource|EString];
	public RequiredQuantityElements getRequiredQuantityAccess() {
		return pRequiredQuantity;
	}
	
	public ParserRule getRequiredQuantityRule() {
		return getRequiredQuantityAccess().getRule();
	}
	
	//enum WorkSequenceType:
	//	startToStart | finishToStart | startToFinish | finishToFinish;
	public WorkSequenceTypeElements getWorkSequenceTypeAccess() {
		return eWorkSequenceType;
	}
	
	public EnumRule getWorkSequenceTypeRule() {
		return getWorkSequenceTypeAccess().getRule();
	}
	
	//EInt ecore::EInt:
	//	'-'? INT;
	public EIntElements getEIntAccess() {
		return pEInt;
	}
	
	public ParserRule getEIntRule() {
		return getEIntAccess().getRule();
	}
	
	//terminal ID:
	//	'^'? ('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*;
	public TerminalRule getIDRule() {
		return gaTerminals.getIDRule();
	}
	
	//terminal INT returns ecore::EInt:
	//	'0'..'9'+;
	public TerminalRule getINTRule() {
		return gaTerminals.getINTRule();
	}
	
	//terminal STRING:
	//	'"' ('\\' . | !('\\' | '"'))* '"' |
	//	"'" ('\\' . | !('\\' | "'"))* "'";
	public TerminalRule getSTRINGRule() {
		return gaTerminals.getSTRINGRule();
	}
	
	//terminal ML_COMMENT:
	//	'/*'->'*/';
	public TerminalRule getML_COMMENTRule() {
		return gaTerminals.getML_COMMENTRule();
	}
	
	//terminal SL_COMMENT:
	//	'//' !('\n' | '\r')* ('\r'? '\n')?;
	public TerminalRule getSL_COMMENTRule() {
		return gaTerminals.getSL_COMMENTRule();
	}
	
	//terminal WS:
	//	' ' | '\t' | '\r' | '\n'+;
	public TerminalRule getWSRule() {
		return gaTerminals.getWSRule();
	}
	
	//terminal ANY_OTHER:
	//	.;
	public TerminalRule getANY_OTHERRule() {
		return gaTerminals.getANY_OTHERRule();
	}
}
