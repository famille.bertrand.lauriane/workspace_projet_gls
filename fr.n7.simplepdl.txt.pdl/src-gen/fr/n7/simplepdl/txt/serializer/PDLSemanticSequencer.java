/*
 * generated by Xtext 2.17.1
 */
package fr.n7.simplepdl.txt.serializer;

import com.google.inject.Inject;
import fr.n7.simplepdl.txt.services.PDLGrammarAccess;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import simplepdl.Guidance;
import simplepdl.RequiredQuantity;
import simplepdl.Ressource;
import simplepdl.SimplepdlPackage;
import simplepdl.WorkDefinition;
import simplepdl.WorkSequence;

@SuppressWarnings("all")
public class PDLSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private PDLGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == SimplepdlPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case SimplepdlPackage.GUIDANCE:
				sequence_Guidance(context, (Guidance) semanticObject); 
				return; 
			case SimplepdlPackage.PROCESS:
				sequence_Process(context, (simplepdl.Process) semanticObject); 
				return; 
			case SimplepdlPackage.REQUIRED_QUANTITY:
				sequence_RequiredQuantity(context, (RequiredQuantity) semanticObject); 
				return; 
			case SimplepdlPackage.RESSOURCE:
				sequence_Ressource(context, (Ressource) semanticObject); 
				return; 
			case SimplepdlPackage.WORK_DEFINITION:
				sequence_WorkDefinition(context, (WorkDefinition) semanticObject); 
				return; 
			case SimplepdlPackage.WORK_SEQUENCE:
				sequence_WorkSequence(context, (WorkSequence) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     ProcessElement returns Guidance
	 *     Guidance returns Guidance
	 *
	 * Constraint:
	 *     text=STRING
	 */
	protected void sequence_Guidance(ISerializationContext context, Guidance semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SimplepdlPackage.Literals.GUIDANCE__TEXT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SimplepdlPackage.Literals.GUIDANCE__TEXT));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getGuidanceAccess().getTextSTRINGTerminalRuleCall_1_0(), semanticObject.getText());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Process returns Process
	 *
	 * Constraint:
	 *     (name=ID processElements+=ProcessElement*)
	 */
	protected void sequence_Process(ISerializationContext context, simplepdl.Process semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     ProcessElement returns RequiredQuantity
	 *     RequiredQuantity returns RequiredQuantity
	 *
	 * Constraint:
	 *     (requester=[WorkDefinition|EString] request=EInt ressource=[Ressource|EString])
	 */
	protected void sequence_RequiredQuantity(ISerializationContext context, RequiredQuantity semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SimplepdlPackage.Literals.REQUIRED_QUANTITY__REQUESTER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SimplepdlPackage.Literals.REQUIRED_QUANTITY__REQUESTER));
			if (transientValues.isValueTransient(semanticObject, SimplepdlPackage.Literals.REQUIRED_QUANTITY__REQUEST) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SimplepdlPackage.Literals.REQUIRED_QUANTITY__REQUEST));
			if (transientValues.isValueTransient(semanticObject, SimplepdlPackage.Literals.REQUIRED_QUANTITY__RESSOURCE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SimplepdlPackage.Literals.REQUIRED_QUANTITY__RESSOURCE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getRequiredQuantityAccess().getRequesterWorkDefinitionEStringParserRuleCall_1_0_1(), semanticObject.eGet(SimplepdlPackage.Literals.REQUIRED_QUANTITY__REQUESTER, false));
		feeder.accept(grammarAccess.getRequiredQuantityAccess().getRequestEIntParserRuleCall_3_0(), semanticObject.getRequest());
		feeder.accept(grammarAccess.getRequiredQuantityAccess().getRessourceRessourceEStringParserRuleCall_5_0_1(), semanticObject.eGet(SimplepdlPackage.Literals.REQUIRED_QUANTITY__RESSOURCE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     ProcessElement returns Ressource
	 *     Ressource returns Ressource
	 *
	 * Constraint:
	 *     (name=EString quantity=EInt)
	 */
	protected void sequence_Ressource(ISerializationContext context, Ressource semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SimplepdlPackage.Literals.RESSOURCE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SimplepdlPackage.Literals.RESSOURCE__NAME));
			if (transientValues.isValueTransient(semanticObject, SimplepdlPackage.Literals.RESSOURCE__QUANTITY) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SimplepdlPackage.Literals.RESSOURCE__QUANTITY));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getRessourceAccess().getNameEStringParserRuleCall_2_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getRessourceAccess().getQuantityEIntParserRuleCall_4_0(), semanticObject.getQuantity());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     ProcessElement returns WorkDefinition
	 *     WorkDefinition returns WorkDefinition
	 *
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_WorkDefinition(ISerializationContext context, WorkDefinition semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SimplepdlPackage.Literals.WORK_DEFINITION__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SimplepdlPackage.Literals.WORK_DEFINITION__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getWorkDefinitionAccess().getNameIDTerminalRuleCall_2_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     ProcessElement returns WorkSequence
	 *     WorkSequence returns WorkSequence
	 *
	 * Constraint:
	 *     (linkType=WorkSequenceType predecessor=[WorkDefinition|ID] successor=[WorkDefinition|ID])
	 */
	protected void sequence_WorkSequence(ISerializationContext context, WorkSequence semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, SimplepdlPackage.Literals.WORK_SEQUENCE__LINK_TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SimplepdlPackage.Literals.WORK_SEQUENCE__LINK_TYPE));
			if (transientValues.isValueTransient(semanticObject, SimplepdlPackage.Literals.WORK_SEQUENCE__PREDECESSOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SimplepdlPackage.Literals.WORK_SEQUENCE__PREDECESSOR));
			if (transientValues.isValueTransient(semanticObject, SimplepdlPackage.Literals.WORK_SEQUENCE__SUCCESSOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SimplepdlPackage.Literals.WORK_SEQUENCE__SUCCESSOR));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getWorkSequenceAccess().getLinkTypeWorkSequenceTypeEnumRuleCall_1_0(), semanticObject.getLinkType());
		feeder.accept(grammarAccess.getWorkSequenceAccess().getPredecessorWorkDefinitionIDTerminalRuleCall_3_0_1(), semanticObject.eGet(SimplepdlPackage.Literals.WORK_SEQUENCE__PREDECESSOR, false));
		feeder.accept(grammarAccess.getWorkSequenceAccess().getSuccessorWorkDefinitionIDTerminalRuleCall_5_0_1(), semanticObject.eGet(SimplepdlPackage.Literals.WORK_SEQUENCE__SUCCESSOR, false));
		feeder.finish();
	}
	
	
}
