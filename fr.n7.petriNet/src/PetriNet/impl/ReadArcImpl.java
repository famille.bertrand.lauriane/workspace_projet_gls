/**
 */
package PetriNet.impl;

import PetriNet.PetriNetPackage;
import PetriNet.ReadArc;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Read Arc</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ReadArcImpl extends ArcImpl implements ReadArc {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReadArcImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PetriNetPackage.Literals.READ_ARC;
	}

} //ReadArcImpl
