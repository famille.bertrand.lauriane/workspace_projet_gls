/**
 */
package PetriNet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Arc</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link PetriNet.Arc#getWeigth <em>Weigth</em>}</li>
 *   <li>{@link PetriNet.Arc#getPredecessor <em>Predecessor</em>}</li>
 *   <li>{@link PetriNet.Arc#getSuccessor <em>Successor</em>}</li>
 * </ul>
 *
 * @see PetriNet.PetriNetPackage#getArc()
 * @model
 * @generated
 */
public interface Arc extends PetriElements {
	/**
	 * Returns the value of the '<em><b>Weigth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Weigth</em>' attribute.
	 * @see #setWeigth(int)
	 * @see PetriNet.PetriNetPackage#getArc_Weigth()
	 * @model required="true"
	 * @generated
	 */
	int getWeigth();

	/**
	 * Sets the value of the '{@link PetriNet.Arc#getWeigth <em>Weigth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Weigth</em>' attribute.
	 * @see #getWeigth()
	 * @generated
	 */
	void setWeigth(int value);

	/**
	 * Returns the value of the '<em><b>Predecessor</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link PetriNet.State#getLinksToSuccessors <em>Links To Successors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predecessor</em>' reference.
	 * @see #setPredecessor(State)
	 * @see PetriNet.PetriNetPackage#getArc_Predecessor()
	 * @see PetriNet.State#getLinksToSuccessors
	 * @model opposite="linksToSuccessors" required="true"
	 * @generated
	 */
	State getPredecessor();

	/**
	 * Sets the value of the '{@link PetriNet.Arc#getPredecessor <em>Predecessor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Predecessor</em>' reference.
	 * @see #getPredecessor()
	 * @generated
	 */
	void setPredecessor(State value);

	/**
	 * Returns the value of the '<em><b>Successor</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link PetriNet.State#getLinksToPredecessors <em>Links To Predecessors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Successor</em>' reference.
	 * @see #setSuccessor(State)
	 * @see PetriNet.PetriNetPackage#getArc_Successor()
	 * @see PetriNet.State#getLinksToPredecessors
	 * @model opposite="linksToPredecessors" required="true"
	 * @generated
	 */
	State getSuccessor();

	/**
	 * Sets the value of the '{@link PetriNet.Arc#getSuccessor <em>Successor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Successor</em>' reference.
	 * @see #getSuccessor()
	 * @generated
	 */
	void setSuccessor(State value);

} // Arc
