/**
 */
package PetriNet;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link PetriNet.State#getName <em>Name</em>}</li>
 *   <li>{@link PetriNet.State#getLinksToSuccessors <em>Links To Successors</em>}</li>
 *   <li>{@link PetriNet.State#getLinksToPredecessors <em>Links To Predecessors</em>}</li>
 * </ul>
 *
 * @see PetriNet.PetriNetPackage#getState()
 * @model abstract="true"
 * @generated
 */
public interface State extends PetriElements {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see PetriNet.PetriNetPackage#getState_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link PetriNet.State#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Links To Successors</b></em>' reference list.
	 * The list contents are of type {@link PetriNet.Arc}.
	 * It is bidirectional and its opposite is '{@link PetriNet.Arc#getPredecessor <em>Predecessor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Links To Successors</em>' reference list.
	 * @see PetriNet.PetriNetPackage#getState_LinksToSuccessors()
	 * @see PetriNet.Arc#getPredecessor
	 * @model opposite="Predecessor"
	 * @generated
	 */
	EList<Arc> getLinksToSuccessors();

	/**
	 * Returns the value of the '<em><b>Links To Predecessors</b></em>' reference list.
	 * The list contents are of type {@link PetriNet.Arc}.
	 * It is bidirectional and its opposite is '{@link PetriNet.Arc#getSuccessor <em>Successor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Links To Predecessors</em>' reference list.
	 * @see PetriNet.PetriNetPackage#getState_LinksToPredecessors()
	 * @see PetriNet.Arc#getSuccessor
	 * @model opposite="Successor"
	 * @generated
	 */
	EList<Arc> getLinksToPredecessors();

} // State
