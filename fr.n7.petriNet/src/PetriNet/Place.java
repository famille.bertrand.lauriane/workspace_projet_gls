/**
 */
package PetriNet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link PetriNet.Place#getNbToken <em>Nb Token</em>}</li>
 * </ul>
 *
 * @see PetriNet.PetriNetPackage#getPlace()
 * @model
 * @generated
 */
public interface Place extends State {
	/**
	 * Returns the value of the '<em><b>Nb Token</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nb Token</em>' attribute.
	 * @see #setNbToken(int)
	 * @see PetriNet.PetriNetPackage#getPlace_NbToken()
	 * @model required="true"
	 * @generated
	 */
	int getNbToken();

	/**
	 * Sets the value of the '{@link PetriNet.Place#getNbToken <em>Nb Token</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nb Token</em>' attribute.
	 * @see #getNbToken()
	 * @generated
	 */
	void setNbToken(int value);

} // Place
