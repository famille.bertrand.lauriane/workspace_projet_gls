/**
 */
package PetriNet;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Petri Elements</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link PetriNet.PetriElements#getPetri <em>Petri</em>}</li>
 * </ul>
 *
 * @see PetriNet.PetriNetPackage#getPetriElements()
 * @model abstract="true"
 * @generated
 */
public interface PetriElements extends EObject {
	/**
	 * Returns the value of the '<em><b>Petri</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link PetriNet.Petri#getPetriElements <em>Petri Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Petri</em>' container reference.
	 * @see #setPetri(Petri)
	 * @see PetriNet.PetriNetPackage#getPetriElements_Petri()
	 * @see PetriNet.Petri#getPetriElements
	 * @model opposite="petriElements" transient="false"
	 * @generated
	 */
	Petri getPetri();

	/**
	 * Sets the value of the '{@link PetriNet.PetriElements#getPetri <em>Petri</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Petri</em>' container reference.
	 * @see #getPetri()
	 * @generated
	 */
	void setPetri(Petri value);

} // PetriElements
