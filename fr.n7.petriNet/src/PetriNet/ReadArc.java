/**
 */
package PetriNet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Read Arc</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see PetriNet.PetriNetPackage#getReadArc()
 * @model
 * @generated
 */
public interface ReadArc extends Arc {
} // ReadArc
