package simplepdl.manip;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import simplepdl.*;
import simplepdl.Process;
import PetriNet.*;

public class SimplePDL2PetriNet {

	public static void main(String[] args) {
		
		//RECUPERER LE MODELE DU SIMPLE PDL 
		
		// Charger le package SimplePDL afin de l'enregistrer dans le registre d'Eclipse.
		SimplepdlPackage packageInstance = SimplepdlPackage.eINSTANCE;
		
		// Enregistrer l'extension ".xmi" comme devant Ãªtre ouverte Ã 
		// l'aide d'un objet "XMIResourceFactoryImpl"
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("simplepdl", new XMIResourceFactoryImpl());
		
		// CrÃ©er un objet resourceSetImpl qui contiendra une ressource EMF (notre modèle de départ)
		ResourceSet resGet = new ResourceSetImpl();

		// Charger la ressource (notre modèle de départ)
		URI modelURID = URI.createURI("../fr.n7.simplepdl.exemple/pdl-sujet.simplepdl");
		Resource resourceD = resGet.getResource(modelURID, true);
		
		// Récupérer le premier élément du modèle de départ (élément racine)
		Process process = (Process) resourceD.getContents().get(0);
		
		// Créer un objet resourceSetImpl qui contiendra une ressource EMF (le modèle d'arrivée)
		ResourceSet resSet = new ResourceSetImpl();

		
		//CREER LE MODELE DU RESEAU DE PETRI
		
		// Charger le package PetriNet afin de l'enregistrer dans le registre d'Eclipse.
		PetriNetPackage packageInstancePetri = PetriNetPackage.eINSTANCE;
		
		// Enregistrer l'extension ".xmi" comme devant Ãªtre ouverte Ã 
		// l'aide d'un objet "XMIResourceFactoryImpl"
		Resource.Factory.Registry reg2 = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m2 = reg2.getExtensionToFactoryMap();
		m2.put("petrinet", new XMIResourceFactoryImpl());
				
		// Définir la ressource (le modèle d'arrivée)
		URI modelURIA = URI.createURI("../fr.n7.petrinet.exemple/pdl-sujet.petrinet");
		Resource resourceA = resSet.createResource(modelURIA);
		
		// La fabrique pour fabriquer les éléments du réseau de Pétri
	    PetriNetFactory myFactory = PetriNetFactory.eINSTANCE;

		// Créer un élément Pétri
		Petri petri = myFactory.createPetri();
		petri.setName(process.getName());
		
		// Ajouter le Process dans le modèle
		resourceA.getContents().add(petri);
		
		//On transforme d'abord toutes les WorkDefinition
		for (Object o : process.getProcessElements()) {
			if (o instanceof WorkDefinition) {
				WorkDefinition wd = (WorkDefinition) o;
				
				// PLACE d'une WorkDefinition
				Place p_idle = myFactory.createPlace();
				p_idle.setName(wd.getName() + "_idle");
				p_idle.setNbToken(1);
				p_idle.setPetri(petri);
				petri.getPetriElements().add(p_idle);
				
				Place p_running = myFactory.createPlace();
				p_running.setName(wd.getName() + "_running");
				p_running.setNbToken(1);
				p_running.setPetri(petri);
				petri.getPetriElements().add(p_running);
				
				Place p_started = myFactory.createPlace();
				p_started.setName(wd.getName() + "_started");
				p_started.setNbToken(1);
				p_started.setPetri(petri);
				petri.getPetriElements().add(p_started);
				
				Place p_finished = myFactory.createPlace();
				p_finished.setName(wd.getName() + "_finished");
				p_finished.setNbToken(1);
				p_finished.setPetri(petri);
				petri.getPetriElements().add(p_finished);
				
				
				// TRANSITION d'une WorkDefinition
				Transition t_start = myFactory.createTransition();
				t_start.setName(wd.getName() + "_start");
				t_start.setPetri(petri);
				petri.getPetriElements().add(t_start);
				
				Transition t_finish = myFactory.createTransition();
				t_finish.setName(wd.getName() + "_finish");
				t_finish.setPetri(petri);
				petri.getPetriElements().add(t_finish);
				
				
				// ARC d'une WorkDefinition
				Arc idle2start = myFactory.createArc();
				idle2start.setWeigth(1);
				idle2start.setPredecessor(p_idle);
				idle2start.setSuccessor(t_start);
				idle2start.setPetri(petri);
				petri.getPetriElements().add(idle2start);
				
				Arc start2running = myFactory.createArc();
				start2running.setWeigth(1);
				start2running.setPredecessor(t_start);
				start2running.setSuccessor(p_running);
				start2running.setPetri(petri);
				petri.getPetriElements().add(start2running);
				
				Arc start2started = myFactory.createArc();
				start2started.setWeigth(1);
				start2started.setPredecessor(t_start);
				start2started.setSuccessor(p_started);
				start2started.setPetri(petri);
				petri.getPetriElements().add(start2started);
				
				Arc running2finish = myFactory.createArc();
				running2finish.setWeigth(1);
				running2finish.setPredecessor(p_running);
				running2finish.setSuccessor(t_finish);
				running2finish.setPetri(petri);
				petri.getPetriElements().add(running2finish);
				
				Arc finish2finished = myFactory.createArc();
				finish2finished.setWeigth(1);
				finish2finished.setPredecessor(t_finish);
				finish2finished.setSuccessor(p_finished);
				finish2finished.setPetri(petri);
				petri.getPetriElements().add(finish2finished);
			}
		}
		
		//On transforme toutes les WorkSequence
		for (Object o : process.getProcessElements()) {
			if (o instanceof WorkSequence) {
				WorkSequence ws = (WorkSequence) o;
						
				// READARC d'une WorkSequence
				Arc readarc = myFactory.createReadArc();
				readarc.setWeigth(1);
				readarc.setPredecessor(getSourceWS(myFactory, petri, ws));
				readarc.setSuccessor(getTargetWS(myFactory, petri, ws));
				readarc.setPetri(petri);
				petri.getPetriElements().add(readarc);
			}
		}

		//On transforme toutes les ressources
		for (Object o : process.getProcessElements()) {
			if (o instanceof Ressource) {
				Ressource res = (Ressource) o;
				
				// PLACE d'une Ressource 
				
				Place p_ressource = myFactory.createPlace();
				p_ressource.setName(res.getName());
				p_ressource.setNbToken(res.getQuantity());
				p_ressource.setPetri(petri);
				petri.getPetriElements().add(p_ressource);
			}
		}
		
		//On transforme toutes les RequiredQuantity
		for (Object o : process.getProcessElements()) {
			if (o instanceof RequiredQuantity) {
				RequiredQuantity rq = (RequiredQuantity) o;
				
				// ARC d'une RequiredQuantity
				
				Arc ressource2start = myFactory.createArc();
				ressource2start.setWeigth(rq.getRequest());
				ressource2start.setPredecessor(getRessource(myFactory, petri, rq));
				ressource2start.setSuccessor(getTransitionRes(myFactory, petri, rq, "_start"));
				ressource2start.setPetri(petri);
				petri.getPetriElements().add(ressource2start);
				
				Arc finish2ressource = myFactory.createArc();
				finish2ressource.setWeigth(rq.getRequest());
				finish2ressource.setPredecessor(getTransitionRes(myFactory, petri, rq, "_finish"));
				finish2ressource.setSuccessor(getRessource(myFactory, petri, rq));
				finish2ressource.setPetri(petri);
				petri.getPetriElements().add(finish2ressource);
			}
		}
		
		// Sauver la ressource
	    try {
	    	resourceA.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Fonction qui renvoie le prédecesseur du ReadArc, transformation d'une WorkSequence
	 * @param myFactory fabrique des éléments du réseau de Pétri
	 * @param petri le pétri à créer
	 * @param ws la WorkSequence à transformer
	 * @return le prédécesseur du futur ReadArc
	 */
	private static State getSourceWS(PetriNetFactory myFactory, Petri petri, WorkSequence ws) {
		WorkSequenceType type = ws.getLinkType();
		WorkDefinition predecessor = ws.getPredecessor();
		
		if (type == WorkSequenceType.START_TO_START_LITERAL ||
				type == WorkSequenceType.START_TO_FINISH_LITERAL) {
			for (Object o : petri.getPetriElements()) {
				if (o instanceof Place) {
					Place place = (Place) o;
					if (place.getName().equals(predecessor.getName() + "_started")) {
						return place;
					}
				}
			}
		} else {
			for (Object o : petri.getPetriElements()) {
				if (o instanceof Place) {
					Place place = (Place) o;
					if (place.getName().equals(predecessor.getName() + "_finished")) {
						return place;
					}
				}
			}
		}
		
		/* Ne doit pas servir car l'élément doit déjà exister */
		Place placeDefault = myFactory.createPlace();
		placeDefault.setName("placeDefault");
		placeDefault.setNbToken(0);
		placeDefault.setPetri(petri);
		return placeDefault;
	}
	
	/**
	 * Fonction qui renvoie le successeur du ReadArc, transformation d'une WorkSequence
	 * @param myFactory fabrique des éléments du réseau de Pétri
	 * @param petri le pétri à créer
	 * @param ws la WorkSequence à transformer
	 * @return le successeur du futur ReadArc
	 */
	private static State getTargetWS(PetriNetFactory myFactory, Petri petri, WorkSequence ws) {
		WorkSequenceType type = ws.getLinkType();
		WorkDefinition successor = ws.getSuccessor();
		
		if (type == WorkSequenceType.START_TO_START_LITERAL ||
				type == WorkSequenceType.FINISH_TO_START_LITERAL) {
			for (Object o : petri.getPetriElements()) {
				if (o instanceof Transition) {
					Transition transition = (Transition) o;
					if (transition.getName().equals(successor.getName() + "_start") ) {
						return transition;
					}
				}
			}
		} else {
			for (Object o : petri.getPetriElements()) {
				if (o instanceof Transition) {
					Transition transition = (Transition) o;
					if (transition.getName().equals(successor.getName() + "_finish")) {
						return transition;
					}
				}
			}
		}
		/* Ne doit pas servir car l'élément doit déjà exister */
		Transition transitionDefault = myFactory.createTransition();
		transitionDefault.setName("transitionDefault");
		transitionDefault.setPetri(petri);
		return transitionDefault;
	}
	
	
	/**
	 * Fonction qui retourne une ressource sous forme d'une place
	 * @param myFactory fabrique des éléments du réseau de Pétri
	 * @param petri le pétri à créer
	 * @param rq la RequiredQuantity à transformer
	 * @return la ressource liée à la RequiredQuantity sous forme de Place
	 */
	private static State getRessource(PetriNetFactory myFactory, Petri petri, RequiredQuantity rq) {
		Ressource res = rq.getRessource();
		for (Object o : petri.getPetriElements()) {
			if (o instanceof Place) {
				Place place = (Place) o;
				if (place.getName().equals(res.getName())) {
					return place;
				}
			}
		}
		/* Ne doit pas servir car l'élément doit déjà exister */
		Place placeDefault = myFactory.createPlace();
		placeDefault.setName("placeDefault");
		placeDefault.setNbToken(0);
		placeDefault.setPetri(petri);
		return placeDefault;
	}
	
	/**
	 * Fonction qui retourne la transition à lier à la ressource
	 * @param myFactory fabrique des éléments du réseau de Pétri
	 * @param petri le pétri à crée
	 * @param rq la RequiredQuantity à transformer
	 * @param texte le nom de la transition
	 * @return la transition à lier à la ressource
	 */
	private static State getTransitionRes(PetriNetFactory myFactory, Petri petri, RequiredQuantity rq, String texte) {
		WorkDefinition wd = rq.getRequester();
		for (Object o : petri.getPetriElements()) {
			if (o instanceof Transition) {
				Transition transition = (Transition) o;
				if (transition.getName().equals(wd.getName() + texte)) {
					return transition;
				}
			}
		}
		/* Ne doit pas servir car l'élément doit déjà exister */
		Transition transitionDefault = myFactory.createTransition();
		transitionDefault.setName("transitionDefault");
		transitionDefault.setPetri(petri);
		return transitionDefault;
	}

}
