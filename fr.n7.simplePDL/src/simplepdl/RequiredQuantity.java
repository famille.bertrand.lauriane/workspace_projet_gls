/**
 */
package simplepdl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Required Quantity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link simplepdl.RequiredQuantity#getRequest <em>Request</em>}</li>
 *   <li>{@link simplepdl.RequiredQuantity#getRessource <em>Ressource</em>}</li>
 *   <li>{@link simplepdl.RequiredQuantity#getRequester <em>Requester</em>}</li>
 * </ul>
 *
 * @see simplepdl.SimplepdlPackage#getRequiredQuantity()
 * @model
 * @generated
 */
public interface RequiredQuantity extends ProcessElement {
	/**
	 * Returns the value of the '<em><b>Request</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Request</em>' attribute.
	 * @see #setRequest(int)
	 * @see simplepdl.SimplepdlPackage#getRequiredQuantity_Request()
	 * @model
	 * @generated
	 */
	int getRequest();

	/**
	 * Sets the value of the '{@link simplepdl.RequiredQuantity#getRequest <em>Request</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Request</em>' attribute.
	 * @see #getRequest()
	 * @generated
	 */
	void setRequest(int value);

	/**
	 * Returns the value of the '<em><b>Ressource</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link simplepdl.Ressource#getRequiredQuantity <em>Required Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ressource</em>' reference.
	 * @see #setRessource(Ressource)
	 * @see simplepdl.SimplepdlPackage#getRequiredQuantity_Ressource()
	 * @see simplepdl.Ressource#getRequiredQuantity
	 * @model opposite="requiredQuantity" required="true"
	 * @generated
	 */
	Ressource getRessource();

	/**
	 * Sets the value of the '{@link simplepdl.RequiredQuantity#getRessource <em>Ressource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ressource</em>' reference.
	 * @see #getRessource()
	 * @generated
	 */
	void setRessource(Ressource value);

	/**
	 * Returns the value of the '<em><b>Requester</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link simplepdl.WorkDefinition#getRequest <em>Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requester</em>' reference.
	 * @see #setRequester(WorkDefinition)
	 * @see simplepdl.SimplepdlPackage#getRequiredQuantity_Requester()
	 * @see simplepdl.WorkDefinition#getRequest
	 * @model opposite="request" required="true"
	 * @generated
	 */
	WorkDefinition getRequester();

	/**
	 * Sets the value of the '{@link simplepdl.RequiredQuantity#getRequester <em>Requester</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requester</em>' reference.
	 * @see #getRequester()
	 * @generated
	 */
	void setRequester(WorkDefinition value);

} // RequiredQuantity
